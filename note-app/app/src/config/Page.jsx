const Page = {
    'SIGN_IN': '/',
    'SIGN_UP': '/sign-up',
    'FORGOT_PASSWORD': '/forgot',
    'HOME': '/home',
    'NOTE': '/view-note',
    'EDIT_NOTE': '/edit-note',
    'REGISTER_NOTE': '/register-note',
    'USER': '/user',
};

export default Page;