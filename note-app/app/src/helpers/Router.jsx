import React from "react";
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import EditNote from "../views/Notes/EditNote";
import ForgotPassword from "../views/Users/ForgotPassword";
import Home from "../views/Notes/Home";
import Page from '../config/Page'
import RegisterNote from "../views/Notes/RegisterNote";
import SignIn from "../views/Users/SignIn";
import SignUp from "../views/Users/SignUp";
import UserInfo from "../views/Users/UserInfo";
import ViewNote from "../views/Notes/ViewNote";

class Router extends React.Component {
    constructor(props) {
        super(props);
        this.state = { Page: Page }
    }

    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path={this.state.Page.SIGN_IN} component={SignIn}></Route>
                    <Route exact path={this.state.Page.SIGN_UP} component={SignUp}></Route>
                    <Route exact path={this.state.Page.FORGOT_PASSWORD} component={ForgotPassword}></Route>
                    <Route exact path={this.state.Page.HOME} component={Home}></Route>
                    <Route exact path={this.state.Page.NOTE} component={ViewNote}></Route>
                    <Route exact path={this.state.Page.EDIT_NOTE} component={EditNote}></Route>
                    <Route exact path={this.state.Page.REGISTER_NOTE} component={RegisterNote}></Route>
                    <Route exact path={this.state.Page.USER} component={UserInfo}></Route>
                </Switch>
            </BrowserRouter>
        );
    }
}

export default Router;