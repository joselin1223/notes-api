import React from 'react';
import {
    Button,
    TextField,
    Grid,
    Container,
    Typography,
} from '@material-ui/core';
import { Link, useHistory } from 'react-router-dom';
import Page from '../../config/Page.jsx'
import '../../assets/css/style.css';

function SignIn() {

    const history = useHistory();

    const login = () => {
        history.push(Page.HOME);
    };

    return (
        <Container component="main" maxWidth="xs">
            <div>
                <Typography component="h1" variant="h5">
                    Login
                </Typography>
                <form noValidate>
                    <Grid container>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="email"
                                label="Email Address"
                                name="email"
                                autoComplete="email"
                                autoFocus
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Button
                                onClick={login}
                                fullWidth
                                variant="contained"
                                color="primary">
                                Sign In
                            </Button>
                        </Grid>
                        <Grid item xs>
                            <Link to={Page.FORGOT_PASSWORD} className="text-decoration-none">{"Forgot password?"}</Link>
                        </Grid>
                        <Grid item>
                            <Link to={Page.SIGN_UP} className="text-decoration-none">{"Don't have an account? Sign Up"}</Link>
                        </Grid>
                    </Grid>
                </form>
            </div>
        </Container>
    );
}

export default SignIn;