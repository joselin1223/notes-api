import React from 'react';
import {
    Button,
    TextField,
    Grid,
    Container,
    Typography,
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import Page from '../../config/Page.jsx'
import '../../assets/css/style.css';

class ForgotPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = { Page: Page };
    }

    render() {
        return (
            <Container component="main" maxWidth="xs">
                <div>
                    <Typography component="h1" variant="h5">
                        Forgot Password
                    </Typography>
                    <form noValidate>
                        <Grid container>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="email"
                                    label="Enter your email address"
                                    name="email"
                                    autoComplete="email"
                                    autoFocus
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Button
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary">
                                    Reset
                                </Button>
                            </Grid>
                            <Grid item xs>
                                <Link to={this.state.Page.SIGN_IN} className="text-decoration-none">Sign In</Link>
                            </Grid>
                            <Grid item>
                                <Link to={this.state.Page.SIGN_UP} className="text-decoration-none">{"Don't have an account? Sign Up"}</Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Container>
        );
    }
}

export default ForgotPassword;