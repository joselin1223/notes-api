import React from 'react';
import {
    Button,
    TextField,
    Grid,
    Container,
    Typography,
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import Page from '../../config/Page.jsx'
import '../../assets/css/style.css';

class SignUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = { Page: Page };
    }

    render() {
        return (
            <Container component="main" maxWidth="xs">
                <div>
                    <Typography component="h1" variant="h5">
                        Register
                    </Typography>
                    <form noValidate>
                        <Grid container>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="email"
                                    label="Email Address"
                                    name="email"
                                    autoComplete="email"
                                    autoFocus
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="email"
                                    label="Name"
                                    name="email"
                                    autoComplete="email"
                                    autoFocus
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    name="password"
                                    label="Password"
                                    type="password"
                                    id="password"
                                    autoComplete="current-password"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    name="password"
                                    label="Password confirm"
                                    type="password"
                                    id="password"
                                    autoComplete="current-password"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Button
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary">
                                    Sign Up
                                </Button>
                            </Grid>
                            <Grid item xs={10}></Grid>
                            <Grid item xs={2}>
                                <Link to={this.state.Page.SIGN_IN} className="text-decoration-none">Sign In</Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Container>
        );
    }
}

export default SignUp;