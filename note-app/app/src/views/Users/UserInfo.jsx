import React from 'react';
import { CssBaseline, Container, Grid, Paper, Button } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import MenuBar from '../../components/MenuBar';
import IconButton from '@material-ui/core/IconButton';
import SaveIcon from '@material-ui/icons/Save';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import AppStyle from '../../assets/css/AppStyle';
import TextField from '@material-ui/core/TextField';
import '../../assets/css/style.css';

function UserInfo() {
    const classes = AppStyle();
    const history = useHistory();

    return (
        <CssBaseline>
            <MenuBar></MenuBar>
            <IconButton onClick={() => history.goBack()}>
                <ArrowBackIcon></ArrowBackIcon>
            </IconButton>
            <div className="margin-top-20">
                <Container>
                    <Grid container spacing={0}>
                        <Grid item xs={12} sm={12}>
                            <Paper className={classes.paper}>
                                <Container>
                                    <div>
                                        <TextField label="Name" defaultValue="Joe" size="small" />
                                    </div>
                                    <div>
                                        <TextField label="Email" defaultValue="Joe@mail.com" size="small" />
                                    </div>
                                    <div>
                                        <TextField label="Password" type="password" size="small" />
                                    </div>
                                    <div>
                                        <TextField label="Confirm Password" type="password" size="small" />
                                    </div>
                                    <Container className="margin-top-20">
                                        <Button variant="contained" color="primary" size="small" startIcon={<SaveIcon />}>
                                            Save
                                        </Button>
                                    </Container>
                                </Container>
                            </Paper>
                        </Grid>
                    </Grid>
                </Container>
            </div>
        </CssBaseline>
    );
}

export default UserInfo;