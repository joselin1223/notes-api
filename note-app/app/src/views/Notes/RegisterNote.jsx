import React from 'react';
import MenuBar from '../../components/MenuBar.jsx';
import { CssBaseline, Container, Grid, Paper, TextareaAutosize, Button } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import SaveIcon from '@material-ui/icons/Save';
import DeleteIcon from '@material-ui/icons/Delete';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import ImageList from '@material-ui/core/ImageList';
import ImageListItem from '@material-ui/core/ImageListItem';
import ImageListItemBar from '@material-ui/core/ImageListItemBar';
import AppStyle from '../../assets/css/AppStyle.jsx';
import '../../assets/css/style.css';

function RegisterNote() {
    const classes = AppStyle();
    const history = useHistory();

    return (
        <CssBaseline>
            <MenuBar></MenuBar>
            <div className="margin-top-20">
                <IconButton onClick={() => history.goBack()}>
                    <ArrowBackIcon></ArrowBackIcon>
                </IconButton>
                <Container>
                    <Grid container spacing={0}>
                        <Grid item xs={5} sm={5} />
                        <Grid item xs={2} sm={2} className="margin-bottom-20">
                            <Button variant="contained" color="primary" size="small" startIcon={<SaveIcon />}>
                                Save
                            </Button>
                        </Grid>
                        <Grid item xs={5} sm={5} />
                    </Grid>
                    <Grid container spacing={0} className="margin-top-20">
                        <Grid item xs={1} sm={1}></Grid>
                        <Grid item xs={10} sm={10}>
                            <Paper className={classes.paper}>
                                <TextareaAutosize aria-label="minimum height" minRows={10} className={classes.widthBy10} placeholder="Minimum 3 rows" />
                            </Paper>
                        </Grid>
                        <Grid item xs={1} sm={1}>
                        </Grid>
                    </Grid>
                    <Grid container spacing={0} className="margin-top-20">
                        <Grid item xs={5} sm={5} />
                        <Grid item xs={2} sm={2}>
                            <input
                                accept="image/*"
                                className={classes.uploadInput}
                                id="contained-button-file"
                                multiple
                                size="small"
                                type="file"
                            />
                            <label htmlFor="contained-button-file">
                                <Button
                                    variant="contained"
                                    color="primary"
                                    component="span"
                                    startIcon={<PhotoCamera />}
                                >
                                    Upload
                                </Button>
                            </label>
                        </Grid>
                        <Grid item xs={5} sm={5} />
                    </Grid>
                    <Grid container spacing={0} className="margin-top-20">
                        <Grid item xs={1} sm={1}></Grid>
                        <Grid item>
                            <ImageList>
                                <ImageListItem>
                                    <img src="https://material-ui.com/static/images/image-list/breakfast.jpg" alt="test" />
                                    <ImageListItemBar
                                        position="top"
                                        actionIcon={
                                            <IconButton className={classes.imageIcon}>
                                                <DeleteIcon />
                                            </IconButton>
                                        }
                                        actionPosition="left"
                                        className={classes.imageTitleBar}
                                    />
                                </ImageListItem>
                            </ImageList>
                        </Grid>
                        <Grid item xs={1} sm={1}></Grid>
                    </Grid>
                </Container>
            </div>
        </CssBaseline>
    );
}

export default RegisterNote;