import React from 'react';
import { CssBaseline, Container, Grid, Paper } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import MenuBar from '../../components/MenuBar';
import DeleteDialog from '../../components/DeleteDialog';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ImageList from '@material-ui/core/ImageList';
import ImageListItem from '@material-ui/core/ImageListItem';
import AppStyle from '../../assets/css/AppStyle';
import Page from '../../config/Page'
import '../../assets/css/style.css';

function ViewNote() {
    const classes = AppStyle();
    const history = useHistory();

    return (
        <CssBaseline>
            <MenuBar></MenuBar>
            <IconButton onClick={() => history.goBack()}>
                <ArrowBackIcon></ArrowBackIcon>
            </IconButton>
            <div className="margin-top-20">
                <Container>
                    <Grid container spacing={0}>
                        <Grid item xs={1} sm={1}>
                            <IconButton onClick={() => history.push(Page.EDIT_NOTE)}>
                                <EditIcon></EditIcon>
                            </IconButton>
                        </Grid>
                        <Grid item xs={10} sm={10}>
                            <Paper className={classes.paper}>
                                View Note
                            </Paper>
                        </Grid>
                        <Grid item xs={1} sm={1}>
                            <DeleteDialog></DeleteDialog>
                        </Grid>
                    </Grid>
                    <Grid container spacing={0} className="margin-top-20">
                        <Grid item xs={1} sm={1}></Grid>
                        <Grid item>
                            <ImageList>
                                <ImageListItem>
                                    <img src="https://material-ui.com/static/images/image-list/breakfast.jpg" alt="test" />
                                </ImageListItem>
                            </ImageList>
                        </Grid>
                        <Grid item xs={1} sm={1}></Grid>
                    </Grid>
                </Container>
            </div>
        </CssBaseline>
    );
}

export default ViewNote;