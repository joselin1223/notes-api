import React from 'react';
import { Grid, Paper, Container, CssBaseline, Tooltip, Fab } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import MenuBar from '../../components/MenuBar';
import DeleteDialog from '../../components/DeleteDialog';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';
import Page from '../../config/Page'
import AppStyle from '../../assets/css/AppStyle';
import '../../assets/css/style.css';

function Home() {
    const classes = AppStyle();
    const history = useHistory();

    return (
        <CssBaseline>
            <MenuBar></MenuBar>
            <div className="margin-top-20">
                <Container>
                    <Grid container spacing={3}>
                        <Grid container item xs={12} sm={6}>
                            <Grid item xs={1} sm={1}>
                                <IconButton onClick={() => history.push(Page.EDIT_NOTE)}>
                                    <EditIcon></EditIcon>
                                </IconButton>
                            </Grid>
                            <Grid item xs={10} sm={10}>
                                <Paper onClick={() => history.push(Page.NOTE)} className={classes.paper}>
                                    Notes
                                </Paper>
                            </Grid>
                            <Grid item xs={1} sm={1}>
                                <DeleteDialog></DeleteDialog>
                            </Grid>
                        </Grid>
                    </Grid>
                </Container>
            </div>
            <Tooltip title="Add" aria-label="add">
                <Fab color="secondary" className={classes.absolute} onClick={() => history.push(Page.REGISTER_NOTE)}>
                    <AddIcon />
                </Fab>
            </Tooltip>
        </CssBaseline>
    );
}

export default Home;