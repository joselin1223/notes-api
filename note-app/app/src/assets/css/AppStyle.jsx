import { makeStyles } from '@material-ui/core/styles';

const AppStyle = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        color: theme.palette.text.secondary,
    },
    absolute: {
        position: 'absolute',
        bottom: theme.spacing(2),
        right: theme.spacing(3),
    },
    widthBy10: {
        width: "100%"
    },
    imageIcon: {
        color: 'white',
    },
    imageTitleBar: {
        background:
            'linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, ' +
            'rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    },
    uploadInput: {
        display: 'none',
    },
}));

export default AppStyle;