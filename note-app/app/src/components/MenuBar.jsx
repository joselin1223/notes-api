import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Menu,
    MenuItem,
    IconButton,
    Typography,
    Toolbar,
    AppBar
} from '@material-ui/core';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Brightness4OutlinedIcon from '@material-ui/icons/Brightness4Outlined';
import { Link, useHistory } from 'react-router-dom';
import Page from '../config/Page.jsx'

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

function MenuBar() {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);
    const history = useHistory();

    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = (event) => {
        setAnchorEl(false);
    };

    const homePage = () => {
        history.push(Page.HOME);
    }

    return (
        <div>
            <AppBar position="static">
                <Toolbar>
                    <Typography onClick={homePage} variant="h6" className={classes.title}>
                        Make a Note
                    </Typography>
                    <div>
                        <IconButton>
                            <Brightness4OutlinedIcon></Brightness4OutlinedIcon>
                        </IconButton>
                        <IconButton
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleMenu}
                            color="inherit"
                        >
                            <AccountCircleIcon />
                        </IconButton>
                        <span>Joe</span>
                        <Menu
                            anchorEl={anchorEl}
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={open}
                            onClose={handleClose}
                        >
                            <MenuItem onClick={handleClose}><Link to={Page.USER} className="text-decoration-none">Profile</Link></MenuItem>
                            <MenuItem onClick={handleClose}><Link to={Page.SIGN_IN} className="text-decoration-none">Logout</Link></MenuItem>
                        </Menu>
                    </div>
                </Toolbar>
            </AppBar>
        </div>
    );
}

export default MenuBar;