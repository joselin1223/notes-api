import React from "react";
import Copyright from './components/Copyright.jsx';
import Router from './helpers/Router.jsx';
import { Box } from '@material-ui/core';

class App extends React.Component {
  render() {
    return (
      <div>
        <Router />
        <Box mt={2}>
          <Copyright />
        </Box>
      </div>
    );
  }
}

export default App;