<?php

namespace App\Model;

use App\Model\KeepNotesConnection;
use Illuminate\Support\Facades\DB;

class Notes extends KeepNotesConnection
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notes';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'description',
        'modified',
        'deleted',
        'deleted_date'
    ];

    /**
     * checkNoteIsExisting
     *
     * @param integer $noteId note id
     *
     * @return boolean
     */
    public function checkNoteIsExisting($noteId)
    {
        $this->Notes = new Notes();
        $isExist = $this->Notes
            ->where(
                [
                    'id' => $noteId,
                    'deleted' => 0,
                ]
            )
            ->first();

        $isExist = is_null($isExist) ? false : true;

        return $isExist;
    }

    /**
     * createNote
     *
     * @param \Illuminate\Http\Request $request request
     * @return array
     */
    public function createNote($request)
    {
        $response['error'] = false;
        try {
            $this->Notes = new Notes();
            $this->Notes->user_id = $request->user_id;
            $this->Notes->description = $request->description;
            $this->Notes->save();
            $response['response'] = $this->Notes->id;
        } catch (\Throwable $th) {
            $response['error'] = true;
            $response['response'] = $th->getMessage();
            return $response;
        }

        return $response;
    }

    /**
     * getUserNotes
     *
     * @param \Illuminate\Http\Request $request request
     * @return array
     */
    public function getUserNotes($request)
    {
        $this->Notes = new Notes();
        $noteList = $this->Notes
            ->select([
                'notes.id',
                'notes.user_id',
                'notes.description',
                'note_images.directory'
            ])
            ->where(
                [
                    'user_id' => $request->user_id,
                    'notes.deleted' => 0,
                ]
            )
            ->where('notes.description', 'LIKE', '%' . $request->search . '%')
            ->leftJoin('note_images', function ($q) {
                $q->on('notes.id', '=', 'note_images.note_id')
                    ->where('note_images.deleted', '=', 0);
            })
            ->offset($request->offset)
            ->limit($request->limit)
            ->orderBy('notes.id', 'DESC')
            ->get();
        $data = collect($noteList)->map(
            function ($entity) {
                return [
                    'id' => $entity->id,
                    'user_id' => $entity->user_id,
                    'description' => $entity->description,
                    'image' => $entity->directory,
                ];
            }
        )
            ->toArray();

        return $data;
    }

    /**
     * getUserNotes
     *
     * @param \Illuminate\Http\Request $request request
     * @return array
     */
    public function getNoteInfo($request)
    {
        $this->Notes = new Notes();
        $noteInfo = $this->Notes
            ->select([
                'notes.id',
                'notes.user_id',
                'notes.description',
                'note_images.directory'
            ])
            ->where(
                [
                    'notes.id' => $request->note_id,
                    'notes.deleted' => 0,
                ]
            )
            ->leftJoin('note_images', function ($q) {
                $q->on('notes.id', '=', 'note_images.note_id')
                    ->where('note_images.deleted', '=', 0);
            })
            ->orderBy('notes.id', 'DESC')
            ->first();
        $data = [];
        if (!empty($noteInfo)) {
            $data = [
                'id' => $noteInfo->id,
                'user_id' => $noteInfo->user_id,
                'description' => $noteInfo->description,
                'image' => $noteInfo->directory,
            ];
        }

        return $data;
    }

    /**
     * updateNote
     *
     * @param \Illuminate\Http\Request $request request
     * @return array
     */
    public function updateNote($request)
    {
        if (is_null($request->description)) {
            return true;
        }
        try {
            $this->Notes = new Notes();
            $this->Notes
                ->where(['id' => $request->note_id])
                ->update(
                    [
                        'description' => $request->description,
                    ]
                );
        } catch (\Throwable $th) {
            return $th->getMessage();
        }

        return true;
    }

    /**
     * deleteNote
     *
     * @param \Illuminate\Http\Request $request request
     * @return array
     */
    public function deleteNote($request)
    {
        try {
            $this->Notes = new Notes();
            $this->Notes
                ->where(
                    [
                        'id' => $request->note_id,
                        'deleted' => 0,
                    ]
                )
                ->update(
                    [
                        'deleted' => 1,
                    ]
                );
        } catch (\Throwable $th) {
            return $th->getMessage();
        }

        return true;
    }

    public function checkNotesAuthorization($request)
    {
        $token = $request->Header('USER-API-KEY');
        $this->Notes = new Notes();
        $noteAuthorized = $this->Notes
            ->select('users.token')
            ->where(
                [
                    'notes.id' => $request->note_id,
                    'notes.deleted' => 0
                ]
            )
            ->join('users', 'users.id', '=', 'notes.user_id')
            ->first();

        return $noteAuthorized->token == $token;
    }
}
