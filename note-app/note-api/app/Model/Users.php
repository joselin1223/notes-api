<?php

namespace App\Model;

use App\Model\KeepNotesConnection;
use App\Http\Controllers\Component\MailComponent;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Hash;

class Users extends KeepNotesConnection
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'app_theme',
        'token',
        'modified',
        'deleted',
        'deleted_date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->Users = new User();
        $this->MailComponent = new MailComponent();
    }

    /**
     * checkEmailAddressIsExisting
     *
     * @param string $email Email Address
     * @return $isExist boolean
     */
    public function checkEmailAddressIsExisting($email)
    {
        $isExist = $this->Users
            ->where(['email' => $email])
            ->first();

        $isExist = is_null($isExist) ? false : true;

        return $isExist;
    }

    /**
     * checkEmailUsability
     *
     * @param \Illuminate\Http\Request $request request
     * @return $isExist boolean
     */
    public function checkEmailUsability($request)
    {
        $userToken = $request->Header('USER-API-KEY');
        $usable = $this->Users
            ->select('email')
            ->where(['token' => $userToken])
            ->first();
        if (!is_null($usable) && $usable->email == $request->email) {
            return false;
        }

        $isExist = $this->checkEmailAddressIsExisting($request->email);
        return $isExist;
    }

    /**
     * checkUserAPITokenIsExisting
     *
     * @param string $userToken token
     * @return $isExist boolean
     */
    public function checkUserAPITokenIsExisting($userToken)
    {
        $isExist = $this->Users
            ->where(['token' => $userToken])
            ->first();

        $isExist = is_null($isExist) ? false : true;

        return $isExist;
    }

    /**
     * checkUserIdAndTokenIsSame
     *
     * @param \Illuminate\Http\Request $request request
     * @return $isExist boolean
     */
    public function checkUserIdAndTokenIsSame($request)
    {
        $userToken = $request->Header('USER-API-KEY');
        $userId = $request->input('user_id');
        $isExist = $this->Users
            ->where(
                [
                    'token' => $userToken,
                    'id' => $userId,
                ]
            )
            ->first();

        $isExist = is_null($isExist) ? false : true;

        return $isExist;
    }

    /**
     * registerUser
     *
     * @param \Illuminate\Http\Request $request request
     * @return $isExist boolean
     */
    public function registerUser($request)
    {
        $userToken = Hash::make(config('const.USER-API-TOKEN'));
        $password = Hash::make($request->password);
        $this->Users->name = $request->name;
        $this->Users->email = $request->email;
        $this->Users->password = $password;
        $this->Users->token = $userToken;

        return $this->Users->save() === true ? true : false;
    }

    /**
     * authenticate
     *
     * @param \Illuminate\Http\Request $request request
     * @return boolean
     */
    public function authenticate($request)
    {
        $users = $this->Users
            ->where(['email' => $request->email])
            ->first();
        // Check password
        if (Hash::check($request->password, $users->password)) {
            $users->token = Hash::make(config('const.USER-API-TOKEN'));
            if ($users->save()) {
                return $users;
            }
        }

        return false;
    }

    /**
     * resetPassword
     *
     * @param \Illuminate\Http\Request $request request
     * @return boolean|string
     */
    public function resetPassword($request)
    {
        $users = $this->Users
            ->where(['email' => $request->email])
            ->first();

        $length = 5;
        $newPassword = substr(
            str_shuffle(
                str_repeat(
                    $x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
                    ceil($length / strlen($x))
                )
            ),
            1,
            $length
        );
        // send email
        $mailResponse = $this->MailComponent->sendNewPassword($newPassword, $users);
        // Update password
        if ($mailResponse === true) {
            $users->password = Hash::make($newPassword);
            $users->save();
        }

        return $mailResponse;
    }

    /**
     * updateUserInfo
     *
     * @param \Illuminate\Http\Request $request request
     * @return boolean
     */
    public function updateUserInfo($request)
    {
        $param = [
            'name' => $request->name,
            'email' => $request->email,
        ];
        if (isset($request->password)) {
            $param['password'] = Hash::make($request->password);
        }
        $token = $request->Header('USER-API-KEY');
        $users = $this->Users
            ->where(['token' => $token])
            ->update($param);

        if ($users) {
            return true;
        }

        return false;
    }

    /**
     * updateUserInfo
     *
     * @param \Illuminate\Http\Request $request request
     * @return boolean|App\Model\Users
     */
    public function getUserInfo($request)
    {
        $users = $this->Users
            ->where(
                [
                    'id' => $request->user_id,
                    'token' => $request->Header('USER-API-KEY'),
                ]
            )
            ->first();

        return is_null($users) ? false : $users;
    }
}
