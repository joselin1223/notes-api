<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KeepNotesConnection extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'keep_notes_connection';
}
