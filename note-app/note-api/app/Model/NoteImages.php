<?php

namespace App\Model;

use App\Model\KeepNotesConnection;
use Illuminate\Support\Facades\File;

class NoteImages extends KeepNotesConnection
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'note_images';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'note_id',
        'directory',
        'modified',
        'deleted',
        'deleted_date'
    ];

    /**
     * saveImages
     *
     * @param \Illuminate\Http\Request $request request
     * @param int $noteId note id
     * @return boolean
     */
    public function saveImages($request, $noteId)
    {
        try {
            // Upload image
            $noteImages = new NoteImages();
            $directory = 'images/note_images';
            $imageName = $noteId . '.' . $request->image->getClientOriginalExtension();
            $request->image->move(public_path($directory), $imageName);
            // Saved to db
            $noteImages->note_id = $noteId;
            $noteImages->directory = "$directory/$imageName";
            $noteImages->save();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }

        return true;
    }

    /**
     * updateImage
     *
     * @param \Illuminate\Http\Request $request request
     * @return boolean
     */
    public function updateImage($request)
    {
        try {
            $this->deleteFileImage($request->note_id);
            // Upload image
            $directory = 'images/note_images';
            $imageName = $request->note_id . '.' . $request->image->getClientOriginalExtension();
            $request->image->move(public_path($directory), $imageName);
            $noteImage = new NoteImages();
            $noteImage->where(['note_id' => $request->note_id])
                ->update(
                    [
                        'directory' => "$directory/$imageName",
                    ]
                );
        } catch (\Throwable $th) {
            return $th->getMessage();
        }

        return true;
    }

    /**
     * deleteFileImage
     *
     * @param int $noteId
     * @return void
     */
    public function deleteFileImage($noteId)
    {
        $noteImage = new NoteImages();
        $imageInfo = $noteImage
            ->where(
                [
                    'note_id' => $noteId,
                    'deleted' => 0
                ]
            )
            ->first();
        if (!empty($imageInfo)) {
            if (File::exists(public_path($imageInfo->directory))) {
                File::delete(public_path($imageInfo->directory));
            }
        }
    }

    /**
     * deleteNoteImages
     *
     * @param \Illuminate\Http\Request $request request
     * @return boolean
     */
    public function deleteNoteImages($request)
    {
        try {
            $this->NoteImages = new NoteImages();
            $this->NoteImages
                ->where(
                    [
                        'note_id' => $request->note_id,
                        'deleted' => 0,
                    ]
                )
                ->update(
                    [
                        'deleted' => 1,
                    ]
                );
        } catch (\Throwable $th) {
            return $th->getMessage();
        }

        return true;
    }
}
