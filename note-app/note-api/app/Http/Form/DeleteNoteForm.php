<?php

namespace App\Http\Form;

use App\Model\Notes;
use App\Model\Users;
use Illuminate\Support\Facades\Validator;

/**
 * DeleteNoteForm
 * validate API inputs
 */
class DeleteNoteForm
{
    /**
     * User Table
     */
    protected $Notes;
    /**
     * User Table
     */
    protected $Users;
    /**
     * __construct
     */
    public function __construct()
    {
        $this->Notes = new Notes();
        $this->Users = new Users();
    }

    /**
     * Validate input
     *
     * @param Request $request
     *
     * @return Array || Boolean
     */
    public function validate($request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'note_id' => [
                    'required',
                    'integer',
                    function ($attribute, $value, $fail) use ($request) {
                        if (!$this->Notes->checkNoteIsExisting($value)) {
                            $fail('Note not found');
                        } else {
                            if (!$this->Notes->checkNotesAuthorization($request)) {
                                $fail('Not to authorized to update.');
                            }
                        }
                    },
                ],
                'user_id' => [
                    'required',
                    'integer',
                    function ($attribute, $value, $fail) use ($request) {
                        if (!$this->Users->checkUserIdAndTokenIsSame($request)) {
                            $fail('Not authorized to use this user.');
                        }
                    },
                ],
            ]
        );
        // Return Message
        if ($validator->fails()) {
            $errors = $validator->messages()->get('*');
            return $errors;
        }

        return false;
    }
}
