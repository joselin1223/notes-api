<?php

namespace App\Http\Form;

use App\Model\Users;
use Illuminate\Support\Facades\Validator;

/**
 * CreateNoteForm
 * validate API inputs
 */
class CreateNoteForm
{
    /**
     * User Table
     */
    protected $Users;
    /**
     * __construct
     */
    public function __construct()
    {
        $this->Users = new Users();
    }

    /**
     * Validate input
     *
     * @param Request $request
     *
     * @return Array || Boolean
     */
    public function validate($request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'user_id' => [
                    'required',
                    'integer',
                    function ($attribute, $value, $fail) use ($request) {
                        if (!$this->Users->checkUserIdAndTokenIsSame($request)) {
                            $fail('Not authorized to use this user.');
                        }
                    },
                ],
                'description' => [
                    'required',
                    function ($attribute, $value, $fail) {
                        if (strlen($value) > 255) {
                            $fail('The ' . $attribute . ' field is must be less than 255 characters.');
                        }
                    },
                ],
                'image' => [
                    'image',
                    'max:5000'
                ],
            ]
        );
        // Return Message
        if ($validator->fails()) {
            $errors = $validator->messages()->get('*');
            return $errors;
        }

        return false;
    }
}
