<?php

namespace App\Http\Form;

use App\Model\Users;
use Illuminate\Support\Facades\Validator;

/**
 * ResetPasswordForm
 * validate resetPassword API inputs
 */
class ResetPasswordForm
{
    /**
     * User Table
     */
    protected $Users;

    /**
     * __construct
     */
    public function __construct()
    {
        $this->Users = new Users();
    }

    /**
     * Validate input
     *
     * @param Request $request
     *
     * @return Array || Boolean
     */
    public function validate($request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'email' => [
                    'required',
                    'email',
                    function ($attribute, $value, $fail) {
                        if (!$this->Users->checkEmailAddressIsExisting($value)) {
                            $fail('The ' . $attribute . ' is not exist.');
                        }
                    },
                ],
            ]
        );
        // Return Message
        if ($validator->fails()) {
            $errors = $validator->messages()->get('*');
            return $errors;
        }

        return false;
    }
}
