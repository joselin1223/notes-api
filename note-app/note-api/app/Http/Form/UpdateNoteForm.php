<?php

namespace App\Http\Form;

use App\Model\Notes;
use App\Model\Users;
use Illuminate\Support\Facades\Validator;

/**
 * UpdateNoteForm
 * validate API inputs
 */
class UpdateNoteForm
{
    /**
     * User Table
     */
    protected $Notes;
    /**
     * User Table
     */
    protected $Users;
    /**
     * __construct
     */
    public function __construct()
    {
        $this->Notes = new Notes();
        $this->Users = new Users();
    }

    /**
     * Validate input
     *
     * @param Request $request
     *
     * @return Array || Boolean
     */
    public function validate($request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'note_id' => [
                    'required',
                    'integer',
                    function ($attribute, $value, $fail) use ($request) {
                        if (!$this->Notes->checkNoteIsExisting($value)) {
                            $fail('Note not found');
                        } else {
                            if (!$this->Notes->checkNotesAuthorization($request)) {
                                $fail('Not to authorized to update.');
                            }
                        }
                    },
                ],
                'description' => [
                    function ($attribute, $value, $fail) {
                        if (strlen($value) > 255) {
                            $fail('The ' . $attribute . ' field is must be less than 255 characters.');
                        }
                    },
                ],
                'image' => [
                    'image',
                    'max:5000'
                ],
                'user_id' => [
                    'required',
                    'integer',
                    function ($attribute, $value, $fail) use ($request) {
                        if (!$this->Users->checkUserIdAndTokenIsSame($request)) {
                            $fail('Not authorized to use this user.');
                        }
                    },
                ],
                'delete_image' => [
                    'integer',
                ]
            ]
        );
        // Return Message
        if ($validator->fails()) {
            $errors = $validator->messages()->get('*');
            return $errors;
        }

        return false;
    }
}
