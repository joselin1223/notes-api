<?php

namespace App\Http\Form;

use App\Model\Users;
use Illuminate\Support\Facades\Validator;

/**
 * GetUserInfoForm
 * validate login API inputs
 */
class GetUserInfoForm
{
    /**
     * User Table
     */
    protected $Users;
    /**
     * __construct
     */
    public function __construct()
    {
        $this->Users = new Users();
    }

    /**
     * Validate input
     *
     * @param Request $request
     *
     * @return Array || Boolean
     */
    public function validate($request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'user_id' => [
                    'required',
                    'integer',
                    function ($attribute, $value, $fail) use ($request) {
                        if (!$this->Users->checkUserIdAndTokenIsSame($request)) {
                            $fail('Not authorized to use this user.');
                        }
                    },
                ],
            ]
        );
        // Return Message
        if ($validator->fails()) {
            $errors = $validator->messages()->get('*');
            return $errors;
        }

        return false;
    }
}
