<?php

namespace App\Http\Form;

use App\Model\Users;
use Illuminate\Support\Facades\Validator;

/**
 * UpdateUserInfoForm
 * validate login API inputs
 */
class UpdateUserInfoForm
{
    /**
     * User Table
     */
    protected $Users;

    /**
     * __construct
     */
    public function __construct()
    {
        $this->Users = new Users();
    }

    /**
     * Validate input
     *
     * @param Request $request
     *
     * @return Array || Boolean
     */
    public function validate($request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'email' => [
                    'required',
                    'email',
                    function ($attribute, $value, $fail) use ($request) {
                        if ($this->Users->checkEmailUsability($request)) {
                            $fail('The ' . $attribute . ' is already exist.');
                        }
                    },
                ],
                'password-confirm' => [
                    function ($attribute, $value, $fail) use ($request) {
                        if (isset($request->all()['password']) && $value != $request->all()['password']) {
                            $fail('The ' . $attribute . ' field does not match in the password.');
                        }
                    },
                ]
            ]
        );
        // Return Message
        if ($validator->fails()) {
            $errors = $validator->messages()->get('*');
            return $errors;
        }

        return false;
    }
}
