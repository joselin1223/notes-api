<?php

namespace App\Http\Form;

use App\Model\Users;
use Illuminate\Support\Facades\Validator;

/**
 * RegisterUserForm
 * validate login API inputs
 */
class RegisterUserForm
{
    /**
     * User Table
     */
    protected $Users;

    /**
     * __construct
     */
    public function __construct()
    {
        $this->Users = new Users();
    }

    /**
     * Validate input
     *
     * @param Request $request
     *
     * @return Array || Boolean
     */
    public function validate($request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'email' => [
                    'required',
                    'email',
                    function ($attribute, $value, $fail) {
                        if ($this->Users->checkEmailAddressIsExisting($value)) {
                            $fail('The ' . $attribute . ' is already exist.');
                        }
                    },
                ],
                'name' => [
                    'required',
                ],
                'password' => [
                    'required',
                ],
                'password-confirm' => [
                    'required',
                    function ($attribute, $value, $fail) use ($request) {
                        if ($value != $request->all()['password']) {
                            $fail('The ' . $attribute . ' field does not match in the password.');
                        }
                    },
                ]
            ]
        );
        // Return Message
        if ($validator->fails()) {
            $errors = $validator->messages()->get('*');
            return $errors;
        }

        return false;
    }
}
