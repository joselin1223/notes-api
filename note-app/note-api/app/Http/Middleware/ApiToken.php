<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Model\Users;
use App\Http\Controllers\Component\ResponsesComponent;

class ApiToken
{
    /**
     * ResponseComponent
     */
    protected $ResponseComponent;
    /**
     * allowApiWithoutToken
     */
    protected $allowApiWithoutToken;
    /**
     * User Table
     */
    protected $Users;

    /**
     * __construct
     */
    public function __construct()
    {
        $this->Users = new Users();
        $this->ResponseComponent = new ResponsesComponent();
        $this->allowApiWithoutToken = [
            'api/users/login',
            'api/users/register',
            'api/users/update/password',
        ];
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        // Identify if API dont need token
        if (in_array(request()->route()->uri, $this->allowApiWithoutToken)) {
            return $response;
        }
        // Return error 401 if invalid token
        if (!Hash::check(config('const.API-TOKEN'), $request->Header('API-KEY'))) {
            return $this->ResponseComponent->authenticationFailed();
        }
        // Return error 401 if user token does not exist
        if (!$this->Users->checkUserAPITokenIsExisting($request->Header('USER-API-KEY'))) {
            return $this->ResponseComponent->userAuthenticationFailed();
        }

        return $response;
    }
}
