<?php

namespace App\Http\Controllers;

use App\Model\Users;
use App\Http\Controllers\Component\ResponsesComponent;
use App\Http\Controllers\Component\MailComponent;
use App\Http\Form\CustomValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Routing\Route;

class UsersController extends Controller
{
    protected $MailComponent;
    protected $ResponseComponent;
    protected $CustomValidator;

    /**
     * __construct
     *
     * @param \Illuminate\Routing\Route $route
     * @param \Illuminate\Http\Request  $request
     *
     * @return void
     */
    public function __construct(Route $route, Request $request)
    {
        parent::__construct($route, $request);
        $this->MailComponent = new MailComponent();
        $this->ResponseComponent = new ResponsesComponent();
        $this->CustomValidator = new CustomValidator();
        $this->Users = new Users();
    }

    /**
     * Login.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function login(Request $request)
    {
        // Validate inputs
        $validationMessage = $this->CustomValidator->validate(
            $request,
            'LoginUserForm'
        );
        // Return response if error found in inputs
        if (!($validationMessage === false)) {
            return $validationMessage;
        }
        // Validate password and get user
        $authenticate = $this->Users->authenticate($request);
        // Return password if incorrect
        if ($authenticate === false) {
            return $this->ResponseComponent->errorResponse('INVALID_PASSWORD');
        }
        // Return data
        $data = [
            'name' => $authenticate->name,
            'email' => $authenticate->email,
            'token' => $authenticate->token,
            'theme' => $authenticate->app_theme,
        ];

        return $this->ResponseComponent->success($data);
    }

    public function logout(Request $request)
    {
        # code...
    }

    /**
     * Register.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function register(Request $request)
    {
        // Validate inputs
        $validationMessage = $this->CustomValidator->validate(
            $request,
            'RegisterUserForm'
        );
        // Return response if error found in inputs
        if (!($validationMessage === false)) {
            return $validationMessage;
        }

        $this->Users->registerUser($request);

        return $this->ResponseComponent->success([]);
    }

    /**
     * updateUserInfo.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function updateUserInfo(Request $request)
    {
        // Validate inputs
        $validationMessage = $this->CustomValidator->validate(
            $request,
            'UpdateUserInfoForm'
        );
        // Return response if error found in inputs
        if (!($validationMessage === false)) {
            return $validationMessage;
        }
        // Update user info
        $this->Users->updateUserInfo($request);

        return $this->ResponseComponent->success([]);
    }

    /**
     * getInfo
     */
    public function getInfo(Request $request)
    {
        // Validate inputs
        $validationMessage = $this->CustomValidator->validate(
            $request,
            'GetUserInfoForm'
        );
        // Return response if error found in inputs
        if (!($validationMessage === false)) {
            return $validationMessage;
        }
        $users = $this->Users->getUserInfo($request);

        $data = [
            'name' => $users->name,
            'email' => $users->email,
            'token' => $users->token,
            'theme' => $users->app_theme,
        ];

        return $this->ResponseComponent->success($data);
    }

    /**
     * resetPassword
     *
     * @param \Illuminate\Http\Request $request
     * @return json
     */
    public function resetPassword(Request $request)
    {
        // Validate inputs
        $validationMessage = $this->CustomValidator->validate(
            $request,
            'ResetPasswordForm'
        );
        // Return response if error found in inputs
        if (!($validationMessage === false)) {
            return $validationMessage;
        }
        $response = $this->Users->resetPassword($request);
        // Return error response of email
        if ($response !== true) {
            return $this->ResponseComponent->errorResponse('EMAIL_NOT_SENT', $response);
        }

        return $this->ResponseComponent->success([]);
    }
}
