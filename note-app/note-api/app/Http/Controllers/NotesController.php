<?php

namespace App\Http\Controllers;

use App\Model\Notes;
use App\Model\NoteImages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Component\ResponsesComponent;
use App\Http\Form\CustomValidator;
use Illuminate\Routing\Route;

class NotesController extends Controller
{
    protected $ResponseComponent;
    protected $CustomValidator;

    /**
     * __construct
     *
     * @param \Illuminate\Routing\Route $route
     * @param \Illuminate\Http\Request  $request
     *
     * @return void
     */
    public function __construct(Route $route, Request $request)
    {
        parent::__construct($route, $request);
        $this->ResponseComponent = new ResponsesComponent();
        $this->CustomValidator = new CustomValidator();
        $this->Notes = new Notes();
        $this->NoteImages = new NoteImages();
    }

    /**
     * createNote
     *
     * @param \Illuminate\Http\Request  $request
     *
     * @return json
     */
    public function createNote(Request $request)
    {
        $validationMessage = $this->CustomValidator->validate(
            $request,
            'CreateNoteForm'
        );

        if (!($validationMessage === false)) {
            return $validationMessage;
        }
        // Create note
        $notes = $this->Notes->createNote($request);
        if ($notes['error'] === true) {
            return $this->ResponseComponent->errorResponse(
                'FAILED_TO_SAVE_NOTE',
                $notes['response']
            );
        }
        // Create image
        if (isset($request->image)) {
            $noteImageResponse = $this->NoteImages->saveImages($request, $notes['response']);
            if ($noteImageResponse !== true) {
                return $this->ResponseComponent->errorResponse(
                    'FAILED_TO_UPLOAD_IMAGE',
                    $noteImageResponse
                );
            }
        }

        return $this->ResponseComponent->success([]);
    }

    /**
     * getUserNotes
     *
     * @param \Illuminate\Http\Request  $request
     *
     * @return json
     */
    public function getUserNotes(Request $request)
    {
        $validationMessage = $this->CustomValidator->validate(
            $request,
            'GetUserNoteForm'
        );

        if (!($validationMessage === false)) {
            return $validationMessage;
        }
        // Get note list
        $noteLists = $this->Notes->getUserNotes($request);
        if (empty($noteLists)) {
            return $this->ResponseComponent->noteNotFound();
        }
        return $this->ResponseComponent->success($noteLists);
    }

    /**
     * getNoteInfo
     *
     * @param \Illuminate\Http\Request  $request
     *
     * @return json
     */
    public function getNoteInfo(Request $request)
    {
        $validationMessage = $this->CustomValidator->validate(
            $request,
            'GetNoteInfoForm'
        );

        if (!($validationMessage === false)) {
            return $validationMessage;
        }
        // Get note list
        $noteLists = $this->Notes->getNoteInfo($request);
        if (empty($noteLists)) {
            return $this->ResponseComponent->noteNotFound();
        }
        return $this->ResponseComponent->success($noteLists);
    }

    /**
     * updateNote
     *
     * @param \Illuminate\Http\Request  $request
     *
     * @return json
     */
    public function updateNote(Request $request)
    {
        $validationMessage = $this->CustomValidator->validate(
            $request,
            'UpdateNoteForm'
        );

        if (!($validationMessage === false)) {
            return $validationMessage;
        }
        $updateNote = $this->Notes->updateNote($request);
        if ($updateNote !== true) {
            return $this->ResponseComponent->errorResponse(
                'FAILED_TO_UPDATE_NOTE',
                $updateNote
            );
        }
        // Create image
        if (isset($request->image)) {
            $noteImageResponse = $this->NoteImages->updateImage($request);
            if ($noteImageResponse !== true) {
                return $this->ResponseComponent->errorResponse(
                    'FAILED_TO_UPLOAD_IMAGE',
                    $noteImageResponse
                );
            }
        }

        if ($request->delete_image == 1) {
            $this->NoteImages->deleteNoteImages($request);
            $this->NoteImages->deleteFileImage($request->note_id);
        }

        return $this->ResponseComponent->success([]);
    }

    /**
     * deleteNote
     *
     * @param \Illuminate\Http\Request  $request
     *
     * @return json
     */
    public function deleteNote(Request $request)
    {
        $validationMessage = $this->CustomValidator->validate(
            $request,
            'DeleteNoteForm'
        );

        if (!($validationMessage === false)) {
            return $validationMessage;
        }
        // Delete note
        $deleteNote = $this->Notes->deleteNote($request);
        if ($deleteNote !== true) {
            return $this->ResponseComponent->errorResponse(
                'FAILED_TO_UPDATE_NOTE',
                $deleteNote
            );
        }
        // Delete note image
        $deleteNoteImage = $this->NoteImages->deleteNoteImages($request);
        if ($deleteNoteImage !== true) {
            return $this->ResponseComponent->errorResponse(
                'FAILED_TO_DELETE_IMAGE',
                $deleteNoteImage
            );
        }
        return $this->ResponseComponent->success([]);
    }
}
