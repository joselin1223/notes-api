<?php

namespace App\Http\Controllers\Component;

use Illuminate\Support\Facades\Log;
use Illuminate\Routing\Route;
use Mail;
use App\Model\Users;

/**
 * MailComponent
 * This is used for giving response
 */
class MailComponent
{
    /**
     * sendNewPassword
     *
     * @param string $password newPassword
     * @param \App\Model\Users $user user
     */
    public function sendNewPassword($password, $user)
    {
        $newPassword['password'] = $password;
        try {
            Mail::send('mail.reset_password', $newPassword, function ($message) use ($user) {
                $message->to($user->email);
                $message->subject('New password');
            });
        } catch (\Throwable $th) {
            return $th->getMessage();
        }

        return true;
    }
}
