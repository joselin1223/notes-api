<?php

namespace App\Http\Controllers\Component;

use Illuminate\Support\Facades\Log;
use Illuminate\Routing\Route;

/**
 * ResponsesComponent
 * This is used for giving response
 */
class ResponsesComponent
{

    /**
     * setResponseBody
     *
     * @param array   $body
     * @param integer $status
     *
     * @return json response
     */
    private function setResponseBody(array $body, int $status)
    {
        $response = response(json_encode($body, JSON_UNESCAPED_UNICODE), $status)
            ->withHeaders(
                [
                    'Content-Type' => 'application/json',
                    'Access-Control-Allow-Origin' => '*',
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => '300',
                    'Pragma' => 'no-cache'
                ]
            );

        if (request()->route() !== null) {
            $logMessages = [
                'URL' => request()->route()->uri,
                'response' => json_encode($body, JSON_UNESCAPED_UNICODE)
            ];

            Log::debug(json_encode($logMessages, false));
        }

        return $response;
    }

    /**
     * validationError
     *
     * @return json response
     */
    public function validationError($entity)
    {
        $status = 400;
        $body = [
            'status_code' => $status,
            'message_id' => 'INVALID_PARAMETERS',
            'message' => 'Invalid Parameters',
            'data' => $entity
        ];

        return $this->setResponseBody($body, $status);
    }

    /**
     * authenticationFailed
     *
     * @return json response
     */
    public function authenticationFailed()
    {
        $status = 401;
        $body = [
            'status' => $status,
            'message_id' => 'AUTHENTICATION_FAILED',
            'message' => 'Authentication Failed'
        ];

        return $this->setResponseBody($body, $status);
    }

    /**
     * userAuthenticationFailed
     *
     * @return json response
     */
    public function userAuthenticationFailed()
    {
        $status = 401;
        $body = [
            'status' => $status,
            'message_id' => 'AUTHENTICATION_FAILED',
            'message' => 'Authentication Failed. User does not exist'
        ];

        return $this->setResponseBody($body, $status);
    }

    /**
     * success
     *
     * @return json response
     */
    public function success(array $entity = [])
    {
        $status = 200;

        $body = [
            'status_code' => $status,
            'message_id' => 'SUCCESS',
            'message' => 'Success'
        ];
        // Add data response if exist
        $body = empty($entity) ? $body : array_merge($body, ['data' => $entity]);

        return $this->setResponseBody($body, $status);
    }

    /**
     * notFound
     *
     * @return json response
     */
    public function notFound()
    {
        $status = 404;

        $body = [
            'status_code' => $status,
            'message_id' => 'NOT_FOUND',
            'message' => 'Not Found'
        ];

        return $this->setResponseBody($body, $status);
    }

    /**
     * requestNotFound
     *
     * @return json response
     */
    public function requestNotFound()
    {
        $status = 404;

        $body = [
            'status_code' => $status,
            'message_id' => 'REQUEST_NOT_FOUND',
            'message' => 'Request Not Found'
        ];

        return $this->setResponseBody($body, $status);
    }

    /**
     * noteNotFound
     *
     * @return json response
     */
    public function noteNotFound()
    {
        $status = 404;

        $body = [
            'status_code' => $status,
            'message_id' => 'NOTE_NOT_FOUND',
            'message' => 'Note Not Found'
        ];

        return $this->setResponseBody($body, $status);
    }

    /**
     * errorResponse
     *
     * @param string $messageId messageId
     * @param string $message message
     *
     * @return json response
     */
    public function errorResponse($messageId, $message = null)
    {
        $message = [
            'INVALID_PASSWORD' => [
                'message_id' => 'INVALID_PASSWORD',
                'message' => 'Invalid Password.',
            ],
            'EMAIL_NOT_SENT' => [
                'message_id' => 'EMAIL_NOT_SENT',
                'message' => $message,
            ],
            'FAILED_TO_SAVE_NOTE' => [
                'message_id' => 'FAILED_TO_SAVE_NOTE',
                'message' => $message,
            ],
            'FAILED_TO_UPDATE_NOTE' => [
                'message_id' => 'FAILED_TO_UPDATE_NOTE',
                'message' => $message,
            ],
            'FAILED_TO_DELETE_NOTE' => [
                'message_id' => 'FAILED_TO_DELETE_NOTE',
                'message' => $message,
            ],
            'FAILED_TO_UPLOAD_IMAGE' => [
                'message_id' => 'FAILED_TO_UPLOAD_IMAGE',
                'message' => $message,
            ],
            'FAILED_TO_DELETE_IMAGE' => [
                'message_id' => 'FAILED_TO_DELETE_IMAGE',
                'message' => $message,
            ],
        ];

        $status = 401;
        $body = [
            'status' => $status,
            'message_id' => $message[$messageId]['message_id'],
            'message' => $message[$messageId]['message']
        ];

        return $this->setResponseBody($body, $status);
    }
}
