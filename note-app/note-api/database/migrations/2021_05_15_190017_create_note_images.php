<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoteImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('note_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('note_id')->index('idx_note_id');
            $table->string('directory')->index('idx_directory')->nullable();
            $table->timestamps();
            $table->integer('deleted')->default(0)->index('idx_deleted');
            $table->dateTime('deleted_date')->index('idx_deleted_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('note_images');
    }
}
