<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->index('idx_name');
            $table->string('email')->index('idx_email');
            $table->string('password')->index('idx_password');
            $table->integer('app_theme')->default(0)->index('idx_app_theme');
            $table->string('token')->index('idx_token')->nullable();
            $table->timestamps();
            $table->integer('deleted')->default(0)->index('idx_deleted');
            $table->dateTime('deleted_date')->index('idx_deleted_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
