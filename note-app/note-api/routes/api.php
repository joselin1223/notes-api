<?php

use Illuminate\Http\Request;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\NotesController;
use App\Http\Controllers\Component\ResponsesComponent;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('users')->group(
    function () {
        // Login
        Route::post('/login', [UsersController::class, 'login']);
        // Logout
        Route::post('/logout', [UsersController::class, 'logout']);
        // Register
        Route::post('/register', [UsersController::class, 'register']);
        // Update User
        Route::put('/', [UsersController::class, 'updateUserInfo']);
        // Get User
        Route::get('/', [UsersController::class, 'getInfo']);
        // Reset password
        Route::put('/update/password', [UsersController::class, 'resetPassword']);
    }
);

Route::prefix('notes')->group(
    function () {
        // Create note
        Route::post('/', [NotesController::class, 'createNote']);
        // Get user notes
        Route::get('/', [NotesController::class, 'getUserNotes']);
        // Get note info
        Route::get('/info', [NotesController::class, 'getNoteInfo']);
        // Update note
        Route::post('/info', [NotesController::class, 'updateNote']);
        // Delete note
        Route::delete('/info', [NotesController::class, 'deleteNote']);
    }
);

Route::fallback(
    function () {
        $this->ResponseComponent = new ResponsesComponent();
        return $this->ResponseComponent->requestNotFound();
    }
);
